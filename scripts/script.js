jQuery(function($) {
  var updateDO = function(e) {
    if (e) {
      var elm = $('.' + e.dataTransfer.getData('Text'));
      $(e.currentTarget).addClass('hascard').prepend(elm.clone());
      e.preventDefault();
    }

    var dlCard      = parseInt($('fieldset#de hr:first-child').data('value'));
    var valueFirst  = parseInt($('fieldset#p1 hr:first-child').data('value'));
    var valueSecond = parseInt($('fieldset#p2 hr:first-child').data('value'));
    if (!dlCard|| !valueFirst|| !valueSecond) {
      $('#dothis span').html(''); 
    } else {
      $('#dothis span').html(calculate(valueFirst, valueSecond, dlCard));
    }
  }

  $('nav hr')
    .on('selectstart', function(e) {
      e.preventDefault();
      return false;
    })
    .each(function(hr) {
      this.addEventListener('dragstart', function(e) {
        e.dataTransfer.setData('Text', e.target.className);
        return false;
      }, false);
    });
  var drop = document.querySelectorAll('fieldset');
  Array.prototype.slice.call(drop).forEach(function(elm,key) {
    elm.addEventListener( 'dragover', cancel);
    elm.addEventListener( 'dragenter', cancel);
    elm.addEventListener('drop', updateDO, false);
  });
  $('fieldset').on('click','input', function() {
    var dlCard = parseInt($('[name=dealer]:checked').val());
    var valueFirst = parseInt($('[name=player1]:checked').val());
    var valueSecond = parseInt($('[name=player2]:checked').val());
    if (!dlCard|| !valueFirst|| !valueSecond) return;
    $('#dothis').html(calculate(valueFirst, valueSecond, dlCard));
  })
  $('.clear').click(function() {
    $('fieldset hr').addClass('clear');
    $('fieldset').removeClass();
    $('fieldset hr').remove();
    updateDO();
  })
});
function calculate(p1,p2,d) {
  // s -stand, S-split, d-double, h-hit
  var split = {
    // double cards
    '1111':'SSSSSSSSSS',
    '1010':'ssssssssss',
    '99' : 'SSSSSSSSss',
    '88' : 'SSSSSSSSSS',
    '77' : 'SSSSSShhhh',
    '66' : 'SSSSShhhhh',
    '55' : 'ddddddddhh',
    '44' : 'hhhhhhhhhh',
    '33' : 'hhSSSShhhh',
    '22' : 'hhSSSShhhh',
    // now aces
    '119': 'ssssssssss',
    '118': 'ssssssssss',
    '117': 'ssdddsshhh',
    '116': 'hhdddhhhhh',
    '115': 'hhdddhhhhh',
    '114': 'hhdddhhhhh',
    '113': 'hhdddhhhhh',
    '112': 'hhdddhhhhh'};
  var total = [
    'hhhhhhddhsssssssss',
    'hhhhhdddhsssssssss',
    'hhhhhdddssssssssss',
    'hhhhhdddssssssssss',
    'hhhhhdddssssssssss',
    'hhhhhhddhhhhhsssss',
    'hhhhhhddhhhhhsssss',
    'hhhhhhddhhhhhsssss',
    'hhhhhhhdhhhhhsssss',
    'hhhhhhhdhhhhhsssss'];
  var key = Math.max(p1,p2).toString() + Math.min(p1,p2).toString();
  var playerTotal = p1 + p2;
  var advice = split[key] ? split[key][d - 2] : total[d - 2][playerTotal - 4]
  return {
    's': 'Stand',
    'S': 'Split',
    'h': 'Hit',
    'd': 'Double (if not allowed, then hit)'}[advice];
}

function cancel(e) {
  if (e.preventDefault) {
    e.preventDefault();
  }
  return false;
}
