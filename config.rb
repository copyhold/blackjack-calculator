# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "styles"
sass_dir = "styles"
images_dir = "images"
javascripts_dir = "scripts"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_style = :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false
sourcemap = true

# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass
http_generated_images_path = "../images"
require 'autoprefixer-rails'
on_stylesheet_saved do |file|
  css = File.read(file)
  prefixed = AutoprefixerRails.process(css, browsers: ["last 3 versions"]).css
  File.open(file, 'w') do |io|
    io << prefixed
  end
end
